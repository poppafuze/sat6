#!/bin/bash

# (c) 2018 Red Hat Inc.
# license: GPL3
#
# use: determines OS version when running bootstrap.py so it can run on any RHEL6,7 machine
# notes: works with Sat 6.4, tested good
#
# assumes: HGs exist in the Sat, like RHEL6-MIG, RHEL7-MIG
# assumes: AKs exist in the Sat, like RHEL6-MIG, RHEL7-MIG that point to useful CVs and subs

#TODO: check that double quote around OSRAW

SAT=mysat64.mydomain.tld
USER=admin         #your Sat 6.x admin-level user
PASSWORD=password  #your password for above
ORG=Organization
LOC=Location

OSRAW=$(cut -d ' ' -f7 /etc/redhat-release)
echo osraw $OSRAW
MAJOR=$(echo $OSRAW | cut -d '.' -f1)
echo major $MAJOR
MINOR=$(echo $OSRAW | cut -d '.' -f2)
echo minor $MINOR
MAJORMINOR=$MAJOR$MINOR
echo majorminor $MAJORMINOR

cp -fp /etc/yum.repos.d/redhat.repo /root/redhat-repo.pre-mig
cp -fp /etc/rhsm/rhsm.conf /root/rhsm.conf.pre-mig
test -f /root/bootstrap.py && rm /root/bootstrap.py
wget --no-check-cert https://$SAT/pub/bootstrap.py
chmod u+x /root/bootstrap.py
# old early 6.4 OS names
#/root/bootstrap.py -s $SAT -o $ORG -L $LOC -a RHEL$MAJOR-MIG -l $USER -p $PASSWORD -g RHEL$MAJOR-MIG -O 'RedHat $OSRAW' --rex --rex-user=root --force
# OS names after 2018 thanksgiving
# Red Hat keeps mangling the OS names
case $MAJOR in
[6])
    /root/bootstrap.py -s $SAT -o $ORG -L $LOC -a RHEL$MAJOR-MIG -l $USER -p $PASSWORD -g RHEL$MAJOR-MIG -O "RedHat $OSRAW" --rex --rex-user=root --force
    ;;
[7])
    /root/bootstrap.py -s $SAT -o $ORG -L $LOC -a RHEL$MAJOR-MIG -l $USER -p $PASSWORD -g RHEL$MAJOR-MIG -O "RHEL Server $OSRAW" --rex --rex-user=root --force
    ;;
esac

# check repo loss...
#  ansible -i /etc/ansible/hosts.mig all -m raw -a 'subscription-manager repos --list-enabled | grep ID' | egrep -e SUCCESS -e ID
# cat redhat-repo.pre-mig | egrep -e '\[' -e enabled
# yum history  #picked 24 in this example
# yum history addon-info 24 config-repos | grep '\['   #would be nice to find date of the sub event and infer the number

