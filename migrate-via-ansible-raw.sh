#!/bin/bash

# notes:  this was done on purpose as ansible raw because 
#         the commands also needed to be used raw in another client system

# usage:  if an ansible host inventory exists of mixed RHEL5,6,7 systems, 
#         they will be universally be migrated to the designated Sat6.4
#         regardless of whether they were Classic, CDN, Sat5, Sat6.x.
#
# prereqs:  create a hosts.mig inventory using some hammer script-fu, like

INVREF=somehosttomigrate.mydomain.tld
MYSAT=mysat64.mydomain.tld

ansible -i hosts.mig $INVREF -m raw -a 'test -f /root/migrate-command-uni-withpuppet.sh && rm /root/migrate-uni.sh'
ansible -i hosts.mig $INVREF -m raw -a "wget --no-check-cert https://$MYSAT/pub/migrate-uni.sh"
ansible -i hosts.mig $INVREF -m raw -a 'chmod u+x /root/migrate-uni.sh'
ansible -i hosts.mig $INVREF -m raw -a '/usr/sbin/subscription-manager clean'
ansible -i hosts.mig $INVREF -m raw -a '/root/migrate-uni.sh'