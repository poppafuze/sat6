# sat6

## Command line instructions
### Git global setup
```
git config --global user.name "poppafuze"
git config --global user.email "poppafuze@gmail.com"
```
### Create a new repository
```
git clone git@gitlab.com:poppafuze/sat6.git
cd sat6
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
### Existing folder or Git repository
```
cd existing_folder
git init
git remote add origin git@gitlab.com:poppafuze/sat6.git
git add .
git commit
git push -u origin master
```