satellite-installer --scenario satellite \
--foreman-initial-organization "" \
--foreman-initial-location "" \
--foreman-proxy-dns-managed=false \
--foreman-proxy-dhcp-managed=false \
--certs-server-cert "/home/me/certs/mysatfqdn-server-cert.pem" \
--certs-server-cert-req "/home/me/certs/mysatfqdn_cert_req.csr" \
--certs-server-key "/home/me/certs/mysatfqdn_no_pass_key.pem" \
--certs-server-ca-cert "/home/me/certs/bundle.pem"