# https://access.redhat.com/solutions/2797091  #How to re-create puppet master certificates?
# https://access.redhat.com/solutions/2107931  #How to use Custom CA certificate to generate certificates puppet master and puppet agents 
#  ...above says it is not possible to use corp certs that derive from their CAs.
# https://puppet.com/docs/puppet/5.3/config_ssl_external_ca.html  #preload CA to clients and disable CRLs

2107931 indicates it is not possible to have the puppet communication use 3rd-party (Corporate) CAs, because:

- the puppet master signs the client certs, which elevates puppet to the level of the corporate CA or whatever intermediary is assigned.  

- Puppet also cannot be an intermediate to an intermediate.  

- All the hostnames would need to become host.somethingpuppetcontrols.corp.tld.  

- An intermediate CA bundle would need to be sent to the agents manually

- CRLs must be disabled on all the puppet items.  

- This disables the puppet CA management service, which means an entire cert distribution infrastructure must be engineered to preload certs before hosts are connected to Satellite (Satellite normally does most of this automatically)

- Satellite must be worked to not automatically stomp on those certs during registration.

Therefore it is considered by the article to be not possible to swap out the certs in the puppet server or clients as used in the Satellite product.  

A solution is conceivable, and would require specific engineering and testing of automated certification.  The last time I saw this attempted, CloudForms was going to pre-create the host entries and command cert generation and push.  

Final risk analysis:  The notion of a puppet server that could invent customer-facing websites never gets past  security.  Having the certs be traceable did not change the need to keep the puppet master safe from compromise, and enhanced its ability to masquerade as other things.  Also, having puppet on a separate cert path enabled a recovery mechanism in the event a CRL was mistakenly issued.  Self-signed was determined to be less risky.
