#!/bin/bash
# this queries the sat api for smart proxies
# 
# here is a typical transaction:
#[myuser@satellite01 ~]$  curl -sku 'myuser' https://localhost/api/smart_proxies | python -m json.tool
#Enter host password for user 'myuser':
#{
#    "error": {
#        "message": "Unable to authenticate user myuser"
#    }
#}
#[myuser@satellite01 ~]$ 
# this means the Sat is there, vs a 404
#
# also can inject password
#curl -sku myuser:mypassword https://localhost/api/smart_proxies | python -m json.tool

curl -sku 'admin' https://localhost/api/smart_proxies | python -m json.tool
