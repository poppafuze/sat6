# ref
#
# https://access.redhat.com/solutions/1273623
# https://access.redhat.com/documentation/en-US/Red_Hat_Satellite/6.2/html-single/Installation_Guide/index.html#configuring_satellite_server_with_custom_server_certificate

PREREQS:
# ...do the following...
# mkdir /root/sat_cert
# cd /root/sat_cert
[-d /root/sat_cert ] || exit 1

cat << 'EOF' >> satellite_csr.conf
[ req ]
default_bits = 4096
prompt = no
encrypt_key = no
default_md = sha256
distinguished_name = dn
req_extensions = req_ext
 
[ dn ]
CN = sat1.njs.orock.us
emailAddress = admin@sat1.mydns.us
O = orock
OU = njs
L = s
ST = NJ
C = US

[ req_ext ]
subjectAltName = DNS: sat1.mydns.us
EOF

openssl genrsa -out /root/sat_cert/satellite_cert_key.pem 4096

openssl req -new \
-config /root/sat_cert/satellite_csr.conf \
-key /root/sat_cert/satellite_cert_key.pem \
-out /root/sat_cert/satellite_cert_csr.pem
