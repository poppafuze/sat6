satellite-installer --scenario satellite \
--foreman-proxy-dhcp true \
--foreman-proxy-dhcp-managed true \
--foreman-proxy-dhcp-interface ens224 \
--foreman-proxy-dhcp-gateway 10.10.10.40 \
--foreman-proxy-dhcp-range "10.10.10.100 10.10.10.199" \
--foreman-proxy-dhcp-nameservers 10.1.1.21 \
--foreman-proxy-tftp true \
--foreman-proxy-tftp-managed true \
--foreman-proxy-tftp-servername $(hostname)